﻿using System.Net.Http;
using AutoMapper;

namespace Lecture1.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        protected static string _host = @$"https://bsa21.azurewebsites.net";

        protected readonly HttpService _httpService;

        protected readonly IMapper _mapper;


        public BaseService()
        {
            _httpService = new HttpService();

            var mapperConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new MappingProfile());
            });

            _mapper = mapperConfig.CreateMapper();
        }
    }
}
