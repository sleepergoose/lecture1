﻿using System;
using System.Linq;

namespace Lecture1.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Getting data... Waiting...");

            try
            {
                Queries queries = new Queries();

                var query1 = queries.GetTasksAmount(authorId: 26);
                var query2 = queries.GetTasksList(performerId: 36);
                var query3 = queries.GetFinishedTasks(performerId: 37);
                var query4 = queries.GetTeamsMembers();
                var query5 = queries.GetUsersWithTasks();
                var query6 = queries.GetUserSummary(userId: 39);
                var query7 = queries.GetProjectSummary();

                Console.WriteLine("Data were loaded");
 
                Console.WriteLine("");



                /* === Output to Console === */

                /* Print query1 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1. Кол-во тасков у проекта конкретного пользователя");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                
                if(query1 != null && query1.Count() != 0)
                {
                    Console.WriteLine("\t{0,-3} | {1, -45} | {2, -14} |", "ID", "Project Name", "Tasks Amount");
                    Console.WriteLine("\t----------------------------------------------------------------------");
                    foreach (var kvp in query1)
                    {
                        Console.WriteLine("\t{0,-3} | {1, -45} | {2, -14} |", kvp.Key.Id, kvp.Key.Name.Substring(0, GetIndex(kvp.Key.Name, 40)), kvp.Value);
                    }
                }
                else
                {
                    Console.WriteLine("\tThe user has no projects");
                }

                Console.WriteLine("");
                Console.WriteLine("");



                /* Print query2 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("2. Список тасков, назначенных на конкретного пользователя");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                
                if(query2 != null && query2.Count() != 0)
                {
                    Console.WriteLine($"\tPerformer: {query2.FirstOrDefault()?.Performer.Id}-{query2.FirstOrDefault()?.Performer.FirstName}");

                    Console.WriteLine("\t{0,-5} | {1, -25} | {2, -50} |", "ID", "Name", "Description");
                    Console.WriteLine("\t----------------------------------------------------------------------------------------");
                    foreach (var task in query2)
                    {
                        Console.WriteLine("\t{0, -5} | {1, -25} | {2, -50} |", task.Id, task.Name.Substring(0, GetIndex(task.Name, 20)),
                            task.Description.Substring(0, GetIndex(task.Description, 40)) + "...");
                    }
                }
                else
                {
                    Console.WriteLine("\tThe user has no tasks");
                }

                Console.WriteLine("");
                Console.WriteLine("");



                /* Print query3 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("3. Список (id, name) из коллекции тасков, которые выполнены в текущем году для конкретного пользователя");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                
                if(query3 != null && query3.Count() != 0)
                {
                    Console.WriteLine("\t{0,-5} | {1, -25} |", "ID", "Task Name");
                    Console.WriteLine("\t-----------------------------------");
                    foreach (var task in query3)
                    {
                        Console.WriteLine("\t{0, -5} | {1, -25} |", task.Id, task.Name.Substring(0, GetIndex(task.Name, 20)));
                    }
                }
                else
                {
                    Console.WriteLine("\tThe user has no tasks");
                }

                Console.WriteLine("");
                Console.WriteLine("");



                /* Print query4 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("4. Cписок (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                Console.WriteLine("\t{0,-5} | {1, -25} | {2, -50} |", "ID", "Task Name", "Teams List");
                Console.WriteLine("\t-------------------------------------------------------------------------------------------------------");

                foreach (var item in query4)
                {
                    Console.WriteLine("\t{0, -5} | {1, -25} | {2, -65} |", item.Id, item.TeamName.Substring(0, GetIndex(item.TeamName, 20)),
                        string.Join(", ", item.Users.Select(u => u.FirstName)).Substring(0, GetIndex(string.Join(", ", item.Users.Select(u => u.FirstName)), 60)) + "...");

                }
                Console.WriteLine("");
                Console.WriteLine("");



                /* Print query5 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("5. Cписок пользователей по алфавиту (по возрастанию) с отсортированными tasks по длине name (по убыванию)");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                Console.WriteLine(string.Join(", ", query5.Select(u => u.FirstName)));
                Console.WriteLine("");
                Console.WriteLine("\tОтсортированный по длине name (по убыванию) список тасков первого пользователя:");

                foreach (var item in query5.FirstOrDefault().Tasks)
                {
                    Console.WriteLine($"\t\t{item.Name}");
                }
                Console.WriteLine("");
                Console.WriteLine("");



                /* Print query6 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("6. User Summary");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");

                if(query6 != null)
                {
                    Console.WriteLine("\t{0,-10} | {1, -20} | {2, -15} | {3, -10} | {4, -35} |", "User", "Last Project", "Project Tasks", "Bad Tasks", "Longest Task");
                    Console.WriteLine("\t----------------------------------------------------------------------------------------------------------");

                    Console.WriteLine("\t{0,-10} | {1, -20} | {2, -15} | {3, -10} | {4, -35} |",
                        query6.User.FirstName, query6.LastProject.Name.Substring(0, GetIndex(query6.LastProject.Name, 20)),
                        query6.LastProjectTasksAmount, query6.BadTasksAmount,
                        $"{query6.LongestTask.Name} ({query6.LongestTask.CreatedAt.ToShortDateString()} - " +
                        $"{query6.LongestTask.FinishedAt?.ToShortDateString() ?? DateTime.Now.ToShortDateString()})");
                }
                else
                {
                    Console.WriteLine("\tThe user has no projects");
                }

                Console.WriteLine("");
                Console.WriteLine("");



                /* Print query7 */
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("7. Project Summary");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");  
                Console.WriteLine("\t{0,-20} | {1, -20} | {2, -20} | {3, -35} |", "Project", "Longest Task", "Shortest Tasks", "Performers Amount");
                Console.WriteLine("\t-----------------------------------------------------------------------------------------------------------");

                var first = query7.FirstOrDefault();

                Console.WriteLine("\t{0,-20} | {1, -20} | {2, -20} | {3, -35} |",
                    first.Project.Name.Substring(0, GetIndex(first.Project.Name, 20)),
                    first.LongestTaskByDescription.Description.Substring(0, GetIndex(first.LongestTaskByDescription.Description, 20)),
                    first.ShortestTaskByName.Name.Substring(0, GetIndex(first.ShortestTaskByName.Name, 20)),
                    first.TeamMemberAmount);

                Console.WriteLine("");
                Console.WriteLine("");

            }
            catch (Exception)
            {
                Console.WriteLine("Data can't be loaded.");
            }

            Console.ReadLine();
        }


        private static int GetIndex(string text, int limit) => 
            text.Length > limit ? limit : text.Length;
    }
}
