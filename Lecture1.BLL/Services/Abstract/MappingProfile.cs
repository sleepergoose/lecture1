﻿using AutoMapper;
using Lecture1.Common.DTO;
using Lecture1.Common.Entities;

namespace Lecture1.BLL.Services.Abstract
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();

            CreateMap<Lecture1.Common.Entities.Task, TaskDTO>();
            CreateMap<TaskDTO, Lecture1.Common.Entities.Task>();
        }
    }
}
