﻿using Lecture1.Common.DTO;
using System.Threading.Tasks;
using Lecture1.BLL.Services.Abstract;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Lecture1.Common.Entities;

namespace Lecture1.BLL.Services
{
    public class UserService : BaseService
    {
        private string url = $"{_host}/api/Users";

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            var response = await _httpService.GetAsync(url);

            var users = JsonConvert.DeserializeObject<ICollection<UserDTO>>(response);

            return _mapper.Map<IEnumerable<User>>(users);
        }


        public async Task<User> GetUser(int id)
        {
            var response = await _httpService.GetAsync($"{url}/{id}");

            var user = JsonConvert.DeserializeObject<UserDTO>(response);

            return _mapper.Map<User>(user);
        }
    }
}
