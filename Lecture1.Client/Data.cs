﻿using Lecture1.BLL.Services;
using Lecture1.Common.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture1.Client
{
    public static class Data
    {

        public static async Task<List<Project>> GetDataStructure()
        {
            ProjectService projectService = new ProjectService();
            TaskService taskService = new TaskService();
            TeamService teamService = new TeamService();
            UserService userService = new UserService();


            try
            {
                var projects = await projectService.GetAllProgects();
                var teams = await teamService.GetAllTeams();
                var users = await userService.GetAllUsers();
                var tasks = await taskService.GetAllTasks();


                users = users.Join(teams, user => user.TeamId, team => team.Id, (user, team) => {
                    user.Team = team;
                    return user;
                });

                users = users.GroupJoin(tasks, user => user.Id, task => task.PerformerId, (user, _tasks) =>
                {
                    user.Tasks = _tasks as ICollection<Lecture1.Common.Entities.Task>;
                    return user;
                });

                teams = teams.GroupJoin(users, team => team.Id, user => user.TeamId, (team, _users) => {
                    team.Members = _users as ICollection<User>;
                    return team;
                });


                tasks = tasks.Join(users, task => task.PerformerId, user => user.Id, (task, user) => {
                    task.Performer = user;
                    return task;
                });


                tasks = tasks.Join(projects, task => task.ProjectId, project => project.Id, (task, project) => {
                    task.Project = project;
                    return task;
                });

                projects = projects.Join(users, project => project.AuthorId, user => user.Id, (project, user) =>
                {
                    project.Author = user;
                    return project;
                });

                projects = projects.Join(teams, project => project.TeamId, team => team.Id, (project, team) =>
                {
                    project.Team = team;
                    return project;
                });

                projects = projects.GroupJoin(tasks, project => project.Id, task => task.ProjectId, (project, _tasks) => {
                    project.Tasks = _tasks as ICollection<Lecture1.Common.Entities.Task>;
                    return project;
                });

                return projects.ToList();

            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine($"Error: {ex.Message}");
                throw new System.Exception();
            }
        }
    }
}
