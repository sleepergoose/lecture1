﻿using System;
using System.Collections.Generic;

namespace Lecture1.Common.Entities
{
    public sealed class Team
    {
        public Team()
        {
            Members = new List<User>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<User> Members { get; set; }
    }
}
