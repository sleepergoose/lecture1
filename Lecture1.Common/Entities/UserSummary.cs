﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1.Common.Entities
{
    public sealed class UserSummary
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksAmount { get; set; }
        public int BadTasksAmount { get; set; }
        public Task LongestTask { get; set; }
    }
}
