﻿using Lecture1.Common.DTO;
using System.Threading.Tasks;
using Lecture1.BLL.Services.Abstract;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System;
using System.Text;
using Lecture1.Common.Entities;

namespace Lecture1.BLL.Services
{
    public class ProjectService : BaseService
    {
        private string url = $"{_host}/api/Projects";

        public async Task<IEnumerable<Project>> GetAllProgects()
        {
            var response = await _httpService.GetAsync(url);

            var projects = JsonConvert.DeserializeObject<ICollection<ProjectDTO>>(response);

            return _mapper.Map<IEnumerable<Project>>(projects);
        }


        public async Task<Project> GetProgect(int id)
        {
            var response = await _httpService.GetAsync($"{url}/{id}");

            var project = JsonConvert.DeserializeObject<ProjectDTO>(response);

            return _mapper.Map<Project>(project);
        }
    }
}
