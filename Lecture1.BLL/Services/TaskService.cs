﻿using Lecture1.Common.DTO;
using System.Threading.Tasks;
using Lecture1.BLL.Services.Abstract;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Lecture1.BLL.Services
{
    public class TaskService : BaseService
    {
        private string url = $"{_host}/api/Tasks";

        public async Task<IEnumerable<Lecture1.Common.Entities.Task>> GetAllTasks()
        {
            var response = await _httpService.GetAsync(url);

            var tasks = JsonConvert.DeserializeObject<ICollection<TaskDTO>>(response);

            return _mapper.Map<IEnumerable<Lecture1.Common.Entities.Task>>(tasks);
        }


        public async Task<Lecture1.Common.Entities.Task> GetTask(int id)
        {
            var response = await _httpService.GetAsync($"{url}/{id}");

            var task = JsonConvert.DeserializeObject<TaskDTO>(response);

            return _mapper.Map<Lecture1.Common.Entities.Task>(task);
        }
    }
}
