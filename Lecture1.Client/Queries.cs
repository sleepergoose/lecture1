﻿using System;
using System.Linq;
using Lecture1.Common.Entities;
using System.Collections.Generic;

namespace Lecture1.Client
{
    public sealed class Queries
    {
        private readonly List<Project> _projects;

        public Queries()
        {
            _projects = Data.GetDataStructure().Result;
        }


        /* -- 7 -- */
        public List<ProjectSummary> GetProjectSummary()
        {
            return _projects.Select(project => 
                 new ProjectSummary {
                    Project = project,
                    LongestTaskByDescription = project.Tasks?.OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                    ShortestTaskByName = project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault(),
                    TeamMemberAmount = (project.Description.Length > 20 || (project.Tasks == null ? 0 : project.Tasks.Count()) < 3)
                                        ? (project.Team == null ? 0 : project.Team.Members.Count()) : 0 
            }).ToList();
        }


        /* -- 6 -- */
        public UserSummary GetUserSummary(int userId)
        {
            return _projects.SelectMany(project => project.Team.Members)
                .Distinct()
                .Where(user => user.Id == userId)
                .Select(user => {

                    var lastUserProject = _projects.Select(p => p)
                        .Where(p => p.Team.Members.Any(m => m.Id == userId))
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault();
                    
                    return new UserSummary
                    {
                        User = user,
                        LastProject = lastUserProject,
                        LastProjectTasksAmount = lastUserProject.Tasks == null ? 0 : lastUserProject.Tasks.Count(),
                        BadTasksAmount = user.Tasks.Where(task => task.FinishedAt == null).Count(),
                        LongestTask = user.Tasks?
                            .OrderByDescending(t => (t.FinishedAt == null ? DateTime.Now : t.FinishedAt) - t.CreatedAt)
                            .FirstOrDefault()
                    };
                }).FirstOrDefault();
        }


        /* -- 5 -- */
        public List<User> GetUsersWithTasks()
        {
            return _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks.Select(task => task.Performer))
                .Distinct()
                .OrderBy(performer => performer.FirstName)
                .Select(performer => new User
                {
                    Id = performer.Id,
                    FirstName = performer.FirstName,
                    BirthDay = performer.BirthDay,
                    Email = performer.Email,
                    LastName = performer.LastName,
                    RegisteredAt = performer.RegisteredAt,
                    Team = performer.Team,
                    TeamId = performer.TeamId,
                    Tasks = performer.Tasks?.OrderByDescending(task => task.Name.Length).ToList()
                }).ToList();
        }


        /* -- 4 -- */
        public List<(int Id, string TeamName, List<User> Users)> GetTeamsMembers()
        {
            /* Несколько неясно 4 задание, потому привожу два запроса */

            // Если нужно получить список команд, ВСЕ участники которых старше 10 лет и отсортировать
            // их по дате регистрации по убыванию, тогда следующий запрос:
            return _projects.Select(p => p.Team)
                .Distinct()
                .Where(t => t.Members.All(p => p.BirthDay.Year < DateTime.Now.Year - 10))
                .Select(team => 
                    (
                        Id: team.Id, 
                        TeamName: team.Name, 
                        Users: team.Members.OrderByDescending(m => m.RegisteredAt).ToList()
                    )
                ).ToList();

            // Если нужно получить список всех команд, исключив из них участников младше 10 лет 
            // и отсортировать их по дате регистрации по убыванию, тогда следующий запрос:

            /*
            return _projects.SelectMany(project => project.Team.Members)
                .Distinct()
                .Where(member => member.BirthDay.Year < DateTime.Now.Year - 10)
                .OrderByDescending(member => member.RegisteredAt)
                .GroupBy(member => member.Team)
                .Select(g => (Id: g.Key.Id, TeamName: g.Key.Name, Users: g.Select(user => user).ToList()))
                .ToList();
            */
        }


        /* -- 3 -- */
        public List<(int Id, string Name)> GetFinishedTasks(int performerId)
        {
            return _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks)
                .Where(task =>
                        task.PerformerId == performerId &&
                        task.FinishedAt != null &&
                        task.FinishedAt.Value.Year == DateTime.Now.Year)
                .Select(task => (id: task.Id, name: task.Name))
                .ToList();
        }        


        /* -- 2 -- */
        public List<Lecture1.Common.Entities.Task> GetTasksList(int performerId)
        {
            return _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks)
                .Where(task => task.PerformerId == performerId && task.Name.Length < 45)
                .ToList();
        }


        /* -- 1 -- */
        public Dictionary<Project, int> GetTasksAmount(int authorId)
        {
            return _projects.Where(project => project.AuthorId == authorId)
                .ToDictionary(key => key, value => (value.Tasks == null ? 0 : value.Tasks.Count()));
        }
    }
}
