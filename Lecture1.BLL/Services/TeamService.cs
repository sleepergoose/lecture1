﻿using Lecture1.Common.DTO;
using System.Threading.Tasks;
using Lecture1.BLL.Services.Abstract;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Lecture1.Common.Entities;

namespace Lecture1.BLL.Services
{
    public class TeamService : BaseService
    {
        private string url = $"{_host}/api/Teams";

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            var response = await _httpService.GetAsync(url);

            var teams = JsonConvert.DeserializeObject<ICollection<TeamDTO>>(response);

            return _mapper.Map<IEnumerable<Team>>(teams);
        }


        public async Task<Team> GetTeam(int id)
        {
            var response = await _httpService.GetAsync($"{url}/{id}");
            
            var team = JsonConvert.DeserializeObject<TeamDTO>(response);
            
            return _mapper.Map<Team>(team);
        }
    }
}
